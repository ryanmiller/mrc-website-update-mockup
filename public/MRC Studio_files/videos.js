var initVideos = function () {
  var players = Array.from(document.querySelectorAll('[data-plyr-provider]')).map(function (p, i) {
    player = new Plyr(p, {
      'hideControls': true,
    });  
    player.on('error', event = function() {
      instance = event;
      console.log(instance);
    });        
    parent = $(p).parents('.plyr-container').attr("data-index", i);
    
    //this may not work but we should try it anyways
    try {
      $(document).foundation();
    }
    catch(err) {
      //shhh...only warnings now
      console.warn(err);
    }    
    return ($(p).id = player);
  });

  plyrFg = $('.bg-plyr').each(function (i, player) {
    $(player).on('click', function () {
      this.style.visibility = 'hidden';
      $(this).siblings('svg').css("visibility", "hidden");
      $(this).siblings('.fg-plyr').css("visibility", "visible");
      try {
        $(document).foundation();
      }
      catch(err) {
          //shhh...only warnings now
          console.warn(err);
        }          
      $(this).parents('.plyr-container[data-index]').each(function () {
        index = $(this).data('index');
        players[index].play();
      });
    });
  });
}

$(document).ready(initVideos());
$(document).on('view-loaded', initVideos());