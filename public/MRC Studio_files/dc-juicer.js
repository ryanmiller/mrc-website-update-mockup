function updateFilters() {
  // add content before list

  if (document.getElementById('j-title') == null) {
    $('.j-filters').prepend('<div class="j-title h3" id="j-title" data-filter="None">FILTER BY: </div>');
  }


  // create script for html
  var social = '<div class="social-grid" id="social-grid"> <div class="row show-for-medium"> <div class="social-icon-list small-12 medium-4 columns"> <span class="h5" style="color:#000">FOLLOW DC:</span><a class="icon-link" href="https://twitter.com/durhamcollege"><span class="show-for-sr">Twitter</span><span class="fa fa-twitter" aria-hidden="true"></span></a><a class="icon-link" href="https://www.facebook.com/durhamcollege/?hc_ref=ARQBNHPvC7jdbdQl2ypE2xaESrK1DTY5dM03mR9vElUD22nlU_egq7BmR5k2nz1DKgc&fref=nf"><span class="show-for-sr">Facebook</span><span class="fa fa-facebook" aria-hidden="true"></span><a class="icon-link" href="https://www.youtube.com/user/DurhamCollege"><span class="show-for-sr">Youtube</span><span class="fa fa-youtube-play" aria-hidden="true"></span></a><a class="icon-link" href="https://www.instagram.com/durhamcollege/"><span class="show-for-sr">Instagram</span><span class="fa fa-instagram" aria-hidden="true"></span></a></div><a class="link-secondary small-12 medium-4 columns" href="/social-media-hub">DC Social Hub</a><a class="link-secondary  small-12 medium-4 columns" href="/social-directory">Social Directory</a></div></div>'

  //add content after list
  if (document.getElementById('social-grid') == null) {
    $('.j-filters').append(social);

  }
  // remove background-color for icon
  $('.juicer-feed ul.filters li').css('cursor', 'auto');
  $('.j-facebook').css('background', 'none');
  $('.j-instagram').css('background', 'none');
  $('.j-linkedin').css('background', 'none');
  $('.j-twitter').css('background', 'none');
  $('.j-linkedin').css('background', 'none');
  $('.all').css('background', 'none');

  // remove center icon
  $('.juicer-feed ul.j-filters').css('-webkit-justify-content', 'none');
  $('.juicer-feed ul.j-filters').css('justify-content', 'none');
  $('.all:before').css('background', 'none');

  // remove croser to the title

  $('.juicer-feed ul.filters j-title').css('cursor', 'grab');



  // remove text from the filter
  $('.juicer-feed ul.filters li.all').text('ALL');
  $('ul.j-filters li.j-linkedin').text('')
  $('ul.j-filters li.j-facebook').text('')
  $('ul.j-filters li.j-linkedin').text('')
  $('ul.j-filters li.j-instagram').text('');
  $('ul.j-filters li.j-twitter').text('');
  if ($(window).width() < 639) {
    $('ul.j-filters li.j-blog').text('');
  }
  // change icon color

  $('.j-facebook').removeAttr('background-color');
}
